const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const CustomerSchema = new Schema({
  _id: {
    type: mongoose.Types.ObjectId,
  },
  fullName: {
    type: String,
    required: true,
  },
  phone: {
    type: String,
    unique: true,
    required: false,
  },
  email: {
    type: String,
    unique: true,
    required: true,
  },
  address: {
    type: String,
    default: "",
  },
  city: {
    type: String,
    default: "",
  },
  country: {
    type: String,
    default: "",
  },
  orders: [
    {
      type: mongoose.Types.ObjectId,
      ref: "order",
    },
  ],
  timeCreated: {
    type: Date,
    default: Date.now(),
  },
  timeUpdated: {
    type: Date,
    default: Date.now(),
  },
});

module.exports = mongoose.model("customer", CustomerSchema);
