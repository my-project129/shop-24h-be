const mongoose = require("mongoose");

const ProductTypeModel = require("../models/ProductTypeModel");

const CreateProductType = (request, response) => {
    let name = request.body.name;
    let description = request.body.description;

    if(!name) {
        return response.status(400).json({
            status: "Bad request",
            message: "Body is not filled"
        })
    }

    ProductTypeModel.create({
        _id: mongoose.Types.ObjectId(),
        name: name,
        description: description
    }, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        } else {
            return response.status(201).json({
                status: "Created",
                data: data
            })
        }
    })
}

const GetAllProductType  = (request, response) => {
    ProductTypeModel.find((error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success",
                data: data
            })
        }
    })
}

const GetProductTypeByID  = (request, response) => {
    let productTypeId = request.params.productTypeId;

    // Kiểm tra productTypeId có phải ObjectId hay không
    if (!mongoose.Types.ObjectId.isValid(productTypeId)) {
        return response.status(400).json({
            status: "Bad request",
            message: "ProductTypeId is not valid"
        })
    }

    ProductTypeModel.findById(productTypeId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        } else {
            if (data) {
                return response.status(200).json({
                    status: "Success",
                    data: data
                })
            } else {
                return response.status(400).json({
                    message: "Not found"
                })
            }
        }
    })
}

const UpdateProductTypeById  = (request, response) => {
    let productTypeId = request.params.productTypeId;

    // Kiểm tra productTypeId có phải ObjectId hay không
    if (!mongoose.Types.ObjectId.isValid(productTypeId)) {
        return response.status(400).json({
            status: "Bad request",
            message: "ProductTypeId is not valid"
        })
    }

    let { name, description } = request.body;
    ProductTypeModel.findByIdAndUpdate(productTypeId, {
        name: name,
        description: description 
    }, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success",
                data: data
            })
        }
    })
}

const DeleteProductTypeById = (request, response) => {
    let productTypeId = request.params.productTypeId;

    // Kiểm tra productTypeId có phải ObjectId hay không
    if(!mongoose.Types.ObjectId.isValid(productTypeId)) {
        return response.status(400).json({
            status: "Bad request",
            message: "ProductTypeId is not valid"
        })
    }

    ProductTypeModel.findByIdAndDelete(productTypeId, (error) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        } else {
            return response.status(204).send()
        }
    })
} 

module.exports = {
    CreateProductType,
    GetAllProductType,
    GetProductTypeByID,
    UpdateProductTypeById,
    DeleteProductTypeById
}