const { response } = require("express");
const mongoose = require("mongoose");
const CustomerModel = require("../models/CustomerModel");

const OrderModel = require("../models/OrderModel");

const createOrder = (request, response) => {
    let { shippedDate, note, orderDetail, cost  } = request.body;

    let customerId = request.params.customerId;


    if(!mongoose.Types.ObjectId.isValid(customerId)) {
        return response.status(400).json({
            status: "Bad request",
            message: "customerId is not valid"
        })
    }

    OrderModel.create({
        _id: mongoose.Types.ObjectId(),
        shippedDate: shippedDate,
        note: note,
        orderDetail: orderDetail,
        cost: cost,
    }, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        } else {
            CustomerModel.findByIdAndUpdate(customerId, {
                $push: { orders: data._id }
            }, (err) => {
                if(err) {
                    return response.status(500).json({
                        status: "Internal server error",
                        message: err.message
                    })
                } else {
                    return response.status(201).json({
                        status: "Created",
                        data: data
                    })
                }
            })
        }
    })
   
}

const getAllOrder = (request, response) => {
    OrderModel.find((error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success",
                data: data
            })
        }
    })
}

const gettAllOrderOfCustomer = (request, response) => {
    let customerId = request.params.customerId;

    if(!mongoose.Types.ObjectId.isValid(customerId)) {
        return response.status(400).json({
            status: "Bad request",
            message: "customerId is not valid"
        })
    }

    CustomerModel.findById(customerId)
        .populate("orders")
        .exec((error, data) => {
            if(error) {
                return response.status(500).json({
                    status: "Internal server error",
                    message: error.message
                })
            } else {
                return response.status(200).json({
                    status: "Success",
                    data: data.orders
                })
            }
        })
}

const getOrderById = (request, response) => {
    let orderId = request.params.orderId;

    if(!mongoose.Types.ObjectId.isValid(orderId)) {
        return response.status(400).json({
            status: "Bad request",
            message: "Review Id is not valid"
        })
    }

    OrderModel.findById(orderId, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        } else {
            if(data) {
                return response.status(200).json({
                    status: "Success",
                    data: data
                })
            } else {
                return response.status(404).json({
                    status: "Not found"
                })
            }
           
        }
    })
}

const updateOrderById = (request, response) => {
    let { shippedDate, note, orderDetail, cost  } = request.body;

    let orderId = request.params.orderId;

    if(!mongoose.Types.ObjectId.isValid(orderId)) {
        return response.status(400).json({
            status: "Bad request",
            message: "orderId is not valid"
        })
    }

    OrderModel.findByIdAndUpdate(orderId, {
        shippedDate: shippedDate,
        note: note,
        orderDetail: orderDetail,
        cost: cost,
    }, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        } else {
            if(data) {
                return response.status(200).json({
                    status: "Success",
                    data: data
                })
            } else {
                return response.status(404).json({
                    status: "Not found"
                })
            }
        }
    })
}

const deleteOrderById = (request, response) => {
    let orderId = request.params.orderId;
    let customerId = request.params.customerId;

    if(!mongoose.Types.ObjectId.isValid(customerId)) {
        return response.status(400).json({
            status: "Bad request",
            message: "customerId is not valid"
        })
    }

    if(!mongoose.Types.ObjectId.isValid(orderId)) {
        return response.status(400).json({
            status: "Bad request",
            message: "orderId is not valid"
        })
    }

    OrderModel.findByIdAndDelete(orderId, (error) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        } else {
            CustomerModel.findByIdAndUpdate(customerId, {
                $pull: { orders: orderId }
            }, (err) => {
                if(err) {
                    return response.status(500).json({
                        status: "Internal server error",
                        message: err.message
                    })
                } else {
                    return response.status(204).json()
                }
            })
        }
    })
}

module.exports = {
    createOrder,
    getAllOrder,
    gettAllOrderOfCustomer,
    getOrderById,
    updateOrderById,
    deleteOrderById
}