const { response } = require("express");
const mongoose = require("mongoose");

const CustomerModel = require("../models/CustomerModel");

const createCustomer = (request, response) => {
  let { fullName, phone, email, address, city, country, orders } = request.body;

  if (!fullName || !phone || !email) {
    return response.status(400).json({
      status: "Bad request",
      message: "Body is not filled",
    });
  }

  CustomerModel.create(
    {
      _id: mongoose.Types.ObjectId(),
      fullName: fullName,
      phone: phone,
      email: email,
      address: address,
      city: city,
      country: country,
      orders: orders,
    },
    (error, data) => {
      if (error) {
        return response.status(500).json({
          status: "Internal server error",
          message: error.message,
        });
      } else {
        return response.status(201).json({
          status: "Created",
          data: data,
        });
      }
    }
  );
};

const GetAllCustomer = (request, response) => {
  CustomerModel.find((error, data) => {
    if (error) {
      return response.status(500).json({
        status: "Internal server error",
        message: error.message,
      });
    } else {
      return response.status(200).json({
        status: "Success",
        data: data,
      });
    }
  });
};

const GetCustomertByID = (request, response) => {
  let customerId = request.params.customerId;

  // Kiểm tra customerId có phải ObjectId hay không
  if (!mongoose.Types.ObjectId.isValid(customerId)) {
    return response.status(400).json({
      status: "Bad request",
      message: "customerId is not valid",
    });
  }

  CustomerModel.findById(customerId, (error, data) => {
    if (error) {
      return response.status(500).json({
        status: "Internal server error",
        message: error.message,
      });
    } else {
      if (data) {
        return response.status(200).json({
          status: "Success",
          data: data,
        });
      } else {
        return response.status(400).json({
          message: "Not found",
        });
      }
    }
  });
};

const UpdateCustomerById = (request, response) => {
  let customerId = request.params.customerId;

  // Kiểm tra customerId có phải ObjectId hay không
  if (!mongoose.Types.ObjectId.isValid(customerId)) {
    return response.status(400).json({
      status: "Bad request",
      message: "customerId is not valid",
    });
  }

  let { fullName, phone, email, address, city, country } = request.body;
  CustomerModel.findByIdAndUpdate(
    customerId,
    {
      fullName: fullName,
      phone: phone,
      email: email,
      address: address,
      city: city,
      country: country,
    },
    (error, data) => {
      if (error) {
        return response.status(500).json({
          status: "Internal server error",
          message: error.message,
        });
      } else {
        return response.status(200).json({
          status: "Success",
          data: data,
        });
      }
    }
  );
};

const DeleteCustomerById = (request, response) => {
  let customerId = request.params.customerId;

  // Kiểm tra customerId có phải ObjectId hay không
  if (!mongoose.Types.ObjectId.isValid(customerId)) {
    return response.status(400).json({
      status: "Bad request",
      message: "customerId is not valid",
    });
  }

  CustomerModel.findByIdAndDelete(customerId, (error) => {
    if (error) {
      return response.status(500).json({
        status: "Internal server error",
        message: error.message,
      });
    } else {
      return response.status(204).send();
    }
  });
};

module.exports = {
  createCustomer,
  GetAllCustomer,
  GetCustomertByID,
  UpdateCustomerById,
  DeleteCustomerById,
};
