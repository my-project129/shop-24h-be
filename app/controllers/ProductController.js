const mongoose = require("mongoose");

const ProductModel = require("../models/ProductModel");

const CreateProduct = (request, response) => {
  //let { name, description, imageUrl, buyPrice, promotionPrice} = request.body;

  let name = request.body.name;
  let description = request.body.description;
  let imageUrl = request.body.imageUrl;
  let buyPrice = request.body.buyPrice;
  let promotionPrice = request.body.promotionPrice;
  let type = request.body.type;

  if (!name || !imageUrl || !buyPrice || !promotionPrice) {
    return response.status(400).json({
      status: "Bad request",
      message: "Body is not filled",
    });
  }

  ProductModel.create(
    {
      _id: mongoose.Types.ObjectId(),
      name: name,
      description: description,
      type: type,
      imageUrl: imageUrl,
      buyPrice: buyPrice,
      promotionPrice: promotionPrice,
    },
    (error, data) => {
      if (error) {
        return response.status(500).json({
          status: "Internal server error",
          message: error.message,
        });
      } else {
        return response.status(201).json({
          status: "Created",
          data: data,
        });
      }
    }
  );
};

const GetAllProduct = (request, response) => {
  const {
    limit,
    name,
    type,
    minPromotionPrice,
    maxPromotionPrice,
    page,
    sort,
  } = request.query;

  const condition = {};

  if (name) {
    const regex = new RegExp(`${name}`);
    condition.name = regex;
  }

  if (type && type !== "Tất cả") {
    condition.type = type;
  }

  if (minPromotionPrice) {
    condition.promotionPrice = {
      ...condition.promotionPrice,
      $gte: minPromotionPrice,
    };
  }

  if (maxPromotionPrice) {
    condition.promotionPrice = {
      ...condition.promotionPrice,
      $lte: maxPromotionPrice,
    };
  }

  ProductModel.find(condition, (error, data) => {
    if (error) {
      return response.status(500).json({
        status: "Internal server error",
        message: error.message,
      });
    } else {
      return response.status(200).json({
        status: "Success",
        data: data,
      });
    }
  })
    .sort({ timeCreated: sort })
    .skip((page - 1) * limit)
    .limit(limit);
};

const GetProductByID = (request, response) => {
  let productId = request.params.productId;

  // Kiểm tra productId có phải ObjectId hay không
  if (!mongoose.Types.ObjectId.isValid(productId)) {
    return response.status(400).json({
      status: "Bad request",
      message: "productId is not valid",
    });
  }

  ProductModel.findById(productId, (error, data) => {
    if (error) {
      return response.status(500).json({
        status: "Internal server error",
        message: error.message,
      });
    } else {
      if (data) {
        return response.status(200).json({
          status: "Success",
          data: data,
        });
      } else {
        return response.status(400).json({
          message: "Not found",
        });
      }
    }
  });
};

const UpdateProductById = (request, response) => {
  let productId = request.params.productId;

  // Kiểm tra productId có phải ObjectId hay không
  if (!mongoose.Types.ObjectId.isValid(productId)) {
    return response.status(400).json({
      status: "Bad request",
      message: "productId is not valid",
    });
  }

  let { name, description, imageUrl, buyPrice, promotionPrice } = request.body;
  ProductModel.findByIdAndUpdate(
    productId,
    {
      name: name,
      description: description,
      imageUrl: imageUrl,
      buyPrice: buyPrice,
      promotionPrice: promotionPrice,
    },
    (error, data) => {
      if (error) {
        return response.status(500).json({
          status: "Internal server error",
          message: error.message,
        });
      } else {
        return response.status(200).json({
          status: "Success",
          data: data,
        });
      }
    }
  );
};

const DeleteProductById = (request, response) => {
  let productId = request.params.productId;

  // Kiểm tra productId có phải ObjectId hay không
  if (!mongoose.Types.ObjectId.isValid(productId)) {
    return response.status(400).json({
      status: "Bad request",
      message: "productId is not valid",
    });
  }

  ProductModel.findByIdAndDelete(productId, (error) => {
    if (error) {
      return response.status(500).json({
        status: "Internal server error",
        message: error.message,
      });
    } else {
      return response.status(204).send();
    }
  });
};

module.exports = {
  CreateProduct,
  GetAllProduct,
  GetProductByID,
  UpdateProductById,
  DeleteProductById,
};
