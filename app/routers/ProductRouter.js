const express = require("express");

const router = express.Router();

const { CreateProduct, GetAllProduct, GetProductByID, UpdateProductById, DeleteProductById } = require("../controllers/ProductController");

router.post("/products", CreateProduct);

router.get("/products", GetAllProduct);

router.get("/products/:productId", GetProductByID);

router.put("/products/:productId", UpdateProductById);

router.delete("/products/:productId", DeleteProductById);


module.exports = router;