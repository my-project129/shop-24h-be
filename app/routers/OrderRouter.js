const express = require("express");

const router = express.Router();

const { createOrder, getAllOrder, gettAllOrderOfCustomer, getOrderById, updateOrderById, deleteOrderById } = require("../controllers/OrderController");

router.post("/customers/:customerId/orders", createOrder);

router.get("/orders", getAllOrder);

router.get("/customers/:customerId/orders", gettAllOrderOfCustomer);

router.get("/orders/:orderId", getOrderById);

router.put("/orders/:orderId", updateOrderById);

router.delete("/customers/:customerId/orders/:orderId", deleteOrderById);

module.exports = router;