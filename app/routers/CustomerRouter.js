const express = require("express");

const router = express.Router();

const { createCustomer, GetAllCustomer, GetCustomertByID, UpdateCustomerById, DeleteCustomerById } = require("../controllers/CustomerController");

router.post("/customers", createCustomer);

router.get("/customers", GetAllCustomer)

router.get("/customers/:customerId", GetCustomertByID);

router.put("/customers/:customerId", UpdateCustomerById);

router.delete("/customers/:customerId", DeleteCustomerById)

module.exports = router;