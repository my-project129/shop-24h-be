const express = require("express");

const router = express.Router();

const { CreateProductType, GetAllProductType, GetProductTypeByID, UpdateProductTypeById, DeleteProductTypeById } = require("../controllers/ProductTypeController");

router.post("/productTypes", CreateProductType);

router.get("/productTypes", GetAllProductType);

router.get("/productTypes/:productTypeId", GetProductTypeByID);

router.put("/productTypes/:productTypeId", UpdateProductTypeById);

router.delete("/productTypes/:productTypeId", DeleteProductTypeById);


module.exports = router;