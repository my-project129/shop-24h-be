const express = require("express");

var mongoose = require("mongoose");

const ProductTypeRouter = require("./app/routers/ProductTypeRouter");
const ProductRouter = require("./app/routers/ProductRouter");
const CustomerRouter = require("./app/routers/CustomerRouter");
const OrderRouter = require("./app/routers/OrderRouter");

// Khởi tạp app express
const app = express();

// Cấu hình để đọc được json
app.use(express.json());

// Cấu hình để đọc được tiếng Việt
app.use(
  express.urlencoded({
    extended: true,
  })
);

//Khai báo cổng
const port = process.env.PORT || 8000;

// mongoose.connect(
//   "mongodb://localhost:27017/CRUD_Shop24h_ProductType",
//   function (error) {
//     if (error) throw error;
//     console.log("Successfully connected");
//   }
// );

mongoose.connect(
  "mongodb+srv://namvu:maynoicaigi123@cluster0.ql0fz.mongodb.net/test?retryWrites=true&w=majority",
  function (error) {
    if (error) throw error;
    console.log("Successfully connected");
  }
);

app.get("/", (request, response) => {
  response.json({
    message: `Xin chào, hôm nay là ngày ${new Date().getDate()}`,
  });
});

app.use(function (req, res, next) {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET, POST, OPTIONS, PUT, PATCH, DELETE"
  );
  res.setHeader(
    "Access-Control-Allow-Headers",
    "X-Requested-With,content-type"
  );
  res.setHeader("Access-Control-Allow-Credentials", true);
  next();
});

//Router level middleware
app.use("/", ProductTypeRouter);
app.use("/", ProductRouter);
app.use("/", CustomerRouter);
app.use("/", OrderRouter);

app.listen(port, () => {
  console.log(`App listening on port ${port}`);
});

// app.listen(port, function () {
//   console.log(
//     "Express server listening on port %d in %s mode",
//     this.address().port,
//     app.settings.env
//   );
// });
